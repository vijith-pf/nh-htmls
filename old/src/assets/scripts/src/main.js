var viewportHeight = $(window).height();
var viewportWidth = $(window).width();

var initFunctions = {
  init: function() {
    initFunctions.sectionBg();
    initFunctions.valuesSwitcher();
    initFunctions.counterUp();
  },
  sectionBg: function () {
    var sectionBg = $('[data-bg]');
    $(sectionBg).each(function () {
        var bg = $(this).attr('data-bg');
        $(this).css("background-image", "url(" + bg + ")").addClass('add-overlay');
    });
  },
  valuesSwitcher: function() {
    $('.values .v-tab-toggle li a').click(function(e){
      var selectedTab = $(this).attr('href');
      $('.values .v-tab-toggle li').removeClass('active');
      $(this).parent().addClass('active');
      $('.values .v-tab-data .info-wrap').removeClass('active');
      $('.values .v-tab-data .info-wrap' + selectedTab).addClass('active');
      e.preventDefault();
    });
  },
  counterUp: function() {
    $('.counter .number').counterUp({
      delay: 15,
      time: 1000
    });
  }
}

var initSliders = {
  heroSlider: function() {
    $("#hero-slider .slider").slick({
      dots: true,
      customPaging: function (slider, i) {
        var title = $(slider.$slides[i]).attr('title');
        if (title != undefined) {
          return '<button class="dots-btn">' + title + '</button>';
        }
      },
      fade: true,
      infinite: false,
      autoplay: true,
      speed: 800,
      arrows: false,
      lazyLoad: 'progressive',
      pauseOnFocus: false,
      pauseOnHover: false,
 
    });
    //$('#hero-slider').slick('slickRemove', $("#hero-slider").slick("getSlick").slideCount - 1 );
    var viewAllSlider = $('#viewAllSlider').html();
    $('#hero-banner .slick-dots').append('<li class="disabled"> ' + viewAllSlider +' </li>');
  },
  heroSliderHide: function () {

  }
}

var initLibraries = {
  slickSliders: function() {
    initSliders.heroSlider();
    initSliders.heroSliderHide();
  },
}

$(document).ready(function() {
    initFunctions.init();
    initLibraries.slickSliders();
});

