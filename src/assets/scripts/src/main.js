var viewportHeight = $(window).height();
var viewportWidth = $(window).width();
var isArabic = false;
if ($('html').attr('lang') == 'ar') {
    isArabic = true;
}


jQuery.fn.scrollCenter = function (elem, speed) {

    // this = #timepicker
    // elem = .active

    var active = jQuery(this).find(elem); // find the active element
    //var activeWidth = active.width(); // get active width
    var activeWidth = active.width() / 2; // get active width center

    //alert(activeWidth)

    //var pos = jQuery('#timepicker .active').position().left; //get left position of active li
    // var pos = jQuery(elem).position().left; //get left position of active li
    //var pos = jQuery(this).find(elem).position().left; //get left position of active li
    var pos = active.position().left + activeWidth; //get left position of active li + center position
    var elpos = jQuery(this).scrollLeft(); // get current scroll position
    var elW = jQuery(this).width(); //get div width
    //var divwidth = jQuery(elem).width(); //get div width
    pos = pos + elpos - elW / 2; // for center position if you want adjust then change this

    jQuery(this).animate({
        scrollLeft: pos
    }, speed == undefined ? 1000 : speed);
    return this;
};

// http://podzic.com/wp-content/plugins/podzic/include/js/podzic.js
jQuery.fn.scrollCenterORI = function (elem, speed) {
    jQuery(this).animate({
        scrollLeft: jQuery(this).scrollLeft() - jQuery(this).offset().left + jQuery(elem).offset().left
    }, speed == undefined ? 1000 : speed);
    return this;
};
var initFunctions = {
    sectionBg: function () {
        var sectionBg = $('[data-bg]');
        $(sectionBg).each(function () {
            var bg = $(this).attr('data-bg');
            $(this).css("background-image", "url(" + bg + ")").addClass('add-overlay');
        });
    },
    counterUp: function () {
        $('.num-counter').counterUp({
            delay: 10,
            time: 1200
        });
    },
    expandShow: function () {
        $(".btn.nh-btn-danger.btn-link").click(function () {
            $(this).parents(".expand-card").toggleClass("show").siblings(".expand-card").removeClass("show");
            $('.expand-card.show .card-body').hover(
              function () {
                 $('html').addClass('disable-scroll');
              }, 
            
              function () {
                 $('html').removeClass('disable-scroll');
              }
            );
        });
    },

    showMore: function () {
        if ($(window).width() >= 767) {

            $(".ldr-scroll").mCustomScrollbar({
                mouseWheel: {
                    preventDefault: false,
                    scrollAmount: 1000,
                    disableOver: ['.expand-card.show .card-body', '.expand-card.show .card-body *']
                }
            })
        }
    },
    showMoreBoxes: function () {
        $(".show-more-boxes").mCustomScrollbar({
            mouseWheel: {
                preventDefault: false
            }
        })
    },

    moreLink: function () {
        var showWord = 56;
        var ellipsestext = " ...";
        var moretext = "Learn More";
        var lesstext = "Show Less";
        $('.more').each(function () {
            var content = $(this).html();
            var textcontent = $(this).text();
            var wordcontent = textcontent.split(" ");
            if (wordcontent.length > showWord) {
                var c = wordcontent.slice(0, showWord).join(" ");
                //var h = content.substr(showChar-1, content.length - showWord);
                var html = '<span class="main-div"><p>' + c + ellipsestext + '</p>' + '</span> <span class="morecontent">' + content + '</span>';
                $(this).html(html);
                $(this).after('<a href="" class="btn nh-btn-danger morelink">' + moretext + '</a>');
            }
        });
        $(".morelink").click(function () {
            if ($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
                $(this).prev().children('.morecontent').fadeToggle(500, function () {
                    $(this).prev().fadeToggle(500);
                });
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
                $(this).prev().children('.main-div').fadeToggle(500, function () {
                    $(this).next().fadeToggle(500);
                });
            }
            //$(this).prev().children().fadeToggle();
            //$(this).parent().prev().prev().fadeToggle(500);
            //$(this).parent().prev().delay(600).fadeToggle(500);
            return false;
        })
    },
    // .morelink
    searchForm: function () {
        const $searchForm = $('.form-search');
        $(document).mouseup(function (e) {
            if (!$searchForm.is(e.target) // if the target of the click isn't the container...
                &&
                $searchForm.has(e.target).length === 0) // ... nor a descendant of the container
            {
                $searchForm.removeClass('active');
            }
        });
        $('#searchButton, .searchButton').click(function (e) {
            $searchForm.toggleClass('active');
            e.preventDefault();
        });
    },
    toggleNavigation: function () {
        $("#toggleNavigation").click(function () {
            $(".collapse.navbar-collapse").addClass("show")
        });
    },
    mobileMenuClose: function () {
        $("#navbarTogglerClose").click(function () {
            $(".collapse.navbar-collapse").removeClass("show")
        });
    },
    toolTip: function () {
        $(".data-listing li").hover(function () {
            // $(this).toggleClass("active")
        });

    },
    tabScroll: function () {

        $(".filter-gallery .nav a").on("click", function () {
            $(".filter-gallery .nav li").removeClass("active-tab");
            $(this).parent().addClass("active-tab");
            var tabActive = $('.filter-gallery .active-tab');
            // console.log(tabActive.position().left);
            //$(".filter-gallery .nav").scrollCenterORI(tabActive , 300);
            /*
            var activeWidth = tabActive.width() / 2;
            var pos = tabActive.position().left;
            $(".filter-gallery .nav").animate({
              scrollLeft: pos
            }, 300);
            */

            $(".filter-gallery .nav").scrollCenter('.active-tab', 300);


        });

    }
}

var initSliders = {
    heroSlider: function () {
        if (isArabic == true) {
            rtll = true;
        } else {
            rtll = false;
        }
        $("#hero-slider").slick({
            dots: true,
            autoplay: true,
            speed: 800,
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            lazyLoad: 'progressive',
            pauseOnFocus: false,
            pauseOnHover: false,
            rtl: rtll,
            infinite: true,
            prevArrow: '<button class="slide-arrow prev-arrow"><i class="icon icon-black-left-arrow"></i></button>',
            nextArrow: '<button class="slide-arrow next-arrow"><i class="icon icon-black-right-arrow"></i></button>',
        });
    },
    heroSlider2: function () {
        if (isArabic == true) {
            rtll = true;
        } else {
            rtll = false;
        }
        if(viewportWidth < 768){
          $(".counter-slider .couter-wrap").slick({
              dots: false,
              autoplay: true,
              speed: 800,
              arrows: false,
              slidesToShow: 4,
              slidesToScroll: 1,
              rtl: rtll,
              lazyLoad: 'progressive',
              pauseOnFocus: false,
              pauseOnHover: false,
              infinite: true,
              autoplaySpeed: 2000,
              variableWidth: true,
              centerMode: true,
              responsive: [{
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                  }
                },
                {
                  breakpoint: 991,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                  }
                }
              ]
            }
          );
        }
    },
    heroSliderHide: function () {

    },
    cardSlider: function () {
        if (isArabic == true) {
            rtll = true;
        } else {
            rtll = false;
        }
        if ($('#news-card-slider .card').length > 3) {
            $('#news-card-slider').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                dots: false,
                autoplay: true,
                centerMode: true,
                focusOnSelect: true,
                variableWidth: true,
                prevArrow: '<button class="slide-arrow prev-arrow"><i class="icon icon-left-arrow"></i></button>',
                nextArrow: '<button class="slide-arrow next-arrow"><i class="icon icon-right-red-arrow"></i></button>',
                rtl: rtll,
                infinite: true,
                responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToScroll: 1,
                        slidesToShow: 1,
                    }
                }, ]
            });
        } else {
            $('#news-card-slider').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                dots: false,
                autoplay: true,
                centerMode: true,
                focusOnSelect: true,
                variableWidth: false,
                infinite: false,
                prevArrow: '<button class="slide-arrow prev-arrow"><i class="icon icon-left-arrow"></i></button>',
                nextArrow: '<button class="slide-arrow next-arrow"><i class="icon icon-right-red-arrow"></i></button>',
                rtl: rtll,
                responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToScroll: 1,
                        slidesToShow: 1,
                    }
                }, ]
            });
        }
    },
    selecter: function () {
        // In your Javascript (external .js resource or <script> tag)
        $(document).ready(function () {
            $('.selector-option').select2();
            $('[data-toggle="tooltip"]').tooltip()
        });
    }
}
var initLibraries = {
    slickSliders: function () {
        initSliders.heroSlider();
        initSliders.heroSliderHide();
        initSliders.heroSlider2();
        initSliders.cardSlider();
        initSliders.selecter();
    },
}

$(document).ready(function () {
    initLibraries.slickSliders();
    initFunctions.sectionBg();
    initFunctions.expandShow();
    initFunctions.showMore();
    initFunctions.showMoreBoxes();
    initFunctions.moreLink();
    initFunctions.counterUp();
    initFunctions.searchForm();
    initFunctions.toggleNavigation();
    initFunctions.mobileMenuClose();
    initFunctions.toolTip();
    initFunctions.tabScroll();
});

$(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
        // $('header').addClass('sticky-header');
    } else {
        // $('header').removeClass('sticky-header');
    }
});


/*
$.fn.fullpage({
  anchors: ['firstPage', 'secondPage', '3rdPage', '4thpage', 'lastPage']
});


var myFullpage = new fullpage('#fullpage', {
  menu: '#menu',
  anchors: ['firstPage', 'secondPage', '3rdPage'],
  sectionsColor: ['#C63D0F', '#1BBC9B', '#7E8F7C'],
  autoScrolling: false,
  licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE'
});
*/

/*
$('#fullpageDisable').fullpage({
  sectionSelector: '.scroll-section',
  normalScrollElements: '.normal-scroll',
  scrollOverflow: true,
  fitToSection: true,
  autoScrolling:true,
  scrollOverflowReset: true
});
*/

/*
$(document).ready(function () {
    if ($('#about-company').length) {
        var stickyTop = $('#about-company').offset().top;
        stickyTop += $('header').offset().top;
        stickyTop += $('.hero-inner').offset().top;

        $(window).scroll(function () {
            var windowTop = $(window).scrollTop();
            if (stickyTop < windowTop) {
                $('#about-company').addClass('sticky');
            } else {
                $('#about-company').removeClass('sticky');
            }
        });
    }
});
*/

$('.radio-button-wrap').each(function(){
  var radioBtns = $(this).find('input[type=radio]');
  $(radioBtns).each(function(){
    if( $(this).is(':checked') ){
      $(this).parents('.wpcf7-list-item').addClass('checked');
    } else {
      $(this).parents('.wpcf7-list-item').removeClass('checked');
    }
  });
});

$('.radio-button-wrap').find('input[type=radio]').change(function(){
  var radioBtns = $(this).parents('.radio-button-wrap').find('input[type=radio]');
  $(radioBtns).each(function(){
    if( $(this).is(':checked') ){
      $(this).parents('.wpcf7-list-item').addClass('checked');
    } else {
      $(this).parents('.wpcf7-list-item').removeClass('checked');
    }
  });
});